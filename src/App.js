import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Tab1 from './components/tab1';
import Tab2 from './components/tab2';
import Tab3 from './components/tab3';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
        currentTab: 1
      
    }
    this.handleClick = this.handleClick.bind(this);
}

handleClick(e){
  this.setState({
    currentTab: e.target.className
  })
}

  render() {

    let body;
    if(this.state.currentTab === "tab1"){
      body = <Tab1 />
    } else if(this.state.currentTab === "tab2"){
      body = <Tab2 />
    } else if (this.state.currentTab === "tab3"){
      body = <Tab3 />
    }

    return (


      <div className="App">
        <div className="tabHeader">
          <h1 className="tab1" onClick={this.handleClick}> Tab 1 </h1>
          <h1 className="tab2" onClick={this.handleClick}> Tab 2 </h1>
          <h1 className="tab3" onClick={this.handleClick}> Tab 3 </h1>
        </div>
        <div className="appBody">
          { body }
        </div>
      </div>
    );
  }
}

export default App;
